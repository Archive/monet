/*
 * Copyright 2009 Intel Corporation.
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms and conditions of the GNU Lesser General Public License,
 * version 2.1, as published by the Free Software Foundation.
 *
 * This program is distributed in the hope it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public License for
 * more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin St - Fifth Floor, Boston, MA 02110-1301 USA.
 *
 * Author: Thomas Wood <thos@gnome.org>
 *
 */

#ifndef _MN_ITEM_H
#define _MN_ITEM_H

#include <glib-object.h>
#include <monet/mn-style.h>

G_BEGIN_DECLS

#define MN_TYPE_ITEM mn_item_get_type()

#define MN_ITEM(obj) \
  (G_TYPE_CHECK_INSTANCE_CAST ((obj), \
  MN_TYPE_ITEM, MnItem))

#define MN_ITEM_CLASS(klass) \
  (G_TYPE_CHECK_CLASS_CAST ((klass), \
  MN_TYPE_ITEM, MnItemClass))

#define MN_IS_ITEM(obj) \
  (G_TYPE_CHECK_INSTANCE_TYPE ((obj), \
  MN_TYPE_ITEM))

#define MN_IS_ITEM_CLASS(klass) \
  (G_TYPE_CHECK_CLASS_TYPE ((klass), \
  MN_TYPE_ITEM))

#define MN_ITEM_GET_CLASS(obj) \
  (G_TYPE_INSTANCE_GET_CLASS ((obj), \
  MN_TYPE_ITEM, MnItemClass))

typedef struct _MnItem MnItem;
typedef struct _MnItemClass MnItemClass;
typedef struct _MnItemPrivate MnItemPrivate;

struct _MnItem
{
  GObject parent;

  MnItemPrivate *priv;
};

struct _MnItemClass
{
  GObjectClass parent_class;
};

GType mn_item_get_type (void);

MnItem *mn_item_new (MnStyle *style, gchar *style_type, gchar *style_class, gchar *style_id, gchar *style_pseudo_class);

G_END_DECLS

#endif /* _MN_ITEM_H */
