/*
 * Copyright 2009 Intel Corporation.
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms and conditions of the GNU Lesser General Public License,
 * version 2.1, as published by the Free Software Foundation.
 *
 * This program is distributed in the hope it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public License for
 * more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin St - Fifth Floor, Boston, MA 02110-1301 USA.
 *
 * Author: Thomas Wood <thos@gnome.org>
 *
 */


#include "mn-item.h"
#include "mn-stylable.h"
#include "mn-color.h"
#include "mn-private.h"

static void mn_stylable_iface_init (MnStylableIface *iface);

G_DEFINE_TYPE_WITH_CODE (MnItem, mn_item, G_TYPE_OBJECT,
                         G_IMPLEMENT_INTERFACE (MN_TYPE_STYLABLE,
                                                mn_stylable_iface_init))

#define ITEM_PRIVATE(o) \
  (G_TYPE_INSTANCE_GET_PRIVATE ((o), MN_TYPE_ITEM, MnItemPrivate))

enum {
    PROP_STYLE = 1,
    PROP_STYLE_TYPE,
    PROP_STYLE_CLASS,
    PROP_STYLE_ID,
    PROP_STYLE_PSEUDO_CLASS
};

struct _MnItemPrivate
{
  MnStyle *style;
  gchar *style_type;
  gchar *style_class;
  gchar *style_id;
  gchar *style_pseudo_class;
};

static void
mn_item_set_style (MnStylable *stylable,
                   MnStyle    *style)
{
  MnItemPrivate *priv = MN_ITEM (stylable)->priv;

  if (priv->style)
    g_object_unref (priv->style);

  priv->style = g_object_ref (style);
}

static MnStyle*
mn_item_get_style (MnStylable *stylable)
{
  MnItemPrivate *priv = MN_ITEM (stylable)->priv;

  return priv->style;
}

static void
mn_stylable_iface_init (MnStylableIface *iface)
{
  MnColor color = { 0, };
  GParamSpec *pspec;

  pspec = mn_param_spec_color ("background-color",
                               "Background Color",
                               "Background colour of the item",
                               &color,
                               MN_PARAM_READWRITE);
  mn_stylable_iface_install_property (iface, MN_TYPE_ITEM, pspec);

  pspec = mn_param_spec_color ("foreground-color",
                               "Foreground Color",
                               "Foreground colour of the item",
                               &color,
                               MN_PARAM_READWRITE);
  mn_stylable_iface_install_property (iface, MN_TYPE_ITEM, pspec);

  pspec = mn_param_spec_color ("highlight-color",
                               "highlight Color",
                               "Highlight colour of the item",
                               &color,
                               MN_PARAM_READWRITE);
  mn_stylable_iface_install_property (iface, MN_TYPE_ITEM, pspec);

  pspec = mn_param_spec_color ("border-color",
                               "Border Color",
                               "Border colour of the item",
                               &color,
                               MN_PARAM_READWRITE);
  mn_stylable_iface_install_property (iface, MN_TYPE_ITEM, pspec);

  /* this could be a boxed type with four integers (top, right, bottom, left) */
  pspec = g_param_spec_int ("border-width",
                            "Border Width",
                            "Width of the border to draw",
                            0, G_MAXINT, 0,
                            MN_PARAM_READWRITE);
  mn_stylable_iface_install_property (iface, MN_TYPE_ITEM, pspec);

  iface->get_style = mn_item_get_style;
  iface->set_style = mn_item_set_style;
}


static void
mn_item_get_property (GObject    *object,
                      guint       property_id,
                      GValue     *value,
                      GParamSpec *pspec)
{
  MnItemPrivate *priv = MN_ITEM (object)->priv;

  switch (property_id)
    {
    case PROP_STYLE:
      g_value_set_object (value, priv->style);
      break;
    case PROP_STYLE_TYPE:
      g_value_set_string (value, priv->style_type);
      break;
    case PROP_STYLE_CLASS:
      g_value_set_string (value, priv->style_class);
      break;
    case PROP_STYLE_ID:
      g_value_set_string (value, priv->style_id);
      break;
    case PROP_STYLE_PSEUDO_CLASS:
      g_value_set_string (value, priv->style_pseudo_class);
      break;
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
    }
}

static void
mn_item_set_property (GObject      *object,
                      guint         property_id,
                      const GValue *value,
                      GParamSpec   *pspec)
{
  MnItemPrivate *priv = MN_ITEM (object)->priv;

  switch (property_id)
    {
    case PROP_STYLE:
      mn_item_set_style (MN_STYLABLE (object), g_value_get_object (value));
      break;
    case PROP_STYLE_TYPE:
      g_free (priv->style_type);
      priv->style_type = g_value_dup_string (value);
      break;
    case PROP_STYLE_CLASS:
      g_free (priv->style_class);
      priv->style_class = g_value_dup_string (value);
      break;
    case PROP_STYLE_ID:
      g_free (priv->style_id);
      priv->style_id = g_value_dup_string (value);
      break;
    case PROP_STYLE_PSEUDO_CLASS:
      g_free (priv->style_pseudo_class);
      priv->style_pseudo_class = g_value_dup_string (value);
      break;
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
    }
}

static void
mn_item_dispose (GObject *object)
{
  G_OBJECT_CLASS (mn_item_parent_class)->dispose (object);
}

static void
mn_item_finalize (GObject *object)
{
  G_OBJECT_CLASS (mn_item_parent_class)->finalize (object);
}

static void
mn_item_class_init (MnItemClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);

  g_type_class_add_private (klass, sizeof (MnItemPrivate));

  object_class->get_property = mn_item_get_property;
  object_class->set_property = mn_item_set_property;
  object_class->dispose = mn_item_dispose;
  object_class->finalize = mn_item_finalize;


  g_object_class_override_property (object_class, PROP_STYLE, "style");
  g_object_class_override_property (object_class, PROP_STYLE_TYPE,
                                    "style-type");
  g_object_class_override_property (object_class, PROP_STYLE_CLASS,
                                    "style-class");
  g_object_class_override_property (object_class, PROP_STYLE_ID,
                                    "style-id");
  g_object_class_override_property (object_class, PROP_STYLE_PSEUDO_CLASS,
                                    "style-pseudo-class");
}

static void
mn_item_init (MnItem *self)
{
  self->priv = ITEM_PRIVATE (self);
}

MnItem *
mn_item_new (MnStyle *style,
             gchar   *style_type,
             gchar   *style_class,
             gchar   *style_id,
             gchar   *style_pseudo_class)
{
  return g_object_new (MN_TYPE_ITEM,
                       "style", style,
                       "style-type", style_type,
                       "style-class", style_class,
                       "style-id", style_id,
                       "style-pseudo-class", style_pseudo_class,
                       NULL);
}
