/*
 * Copyright 2009 Intel Corporation.
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms and conditions of the GNU Lesser General Public License,
 * version 2.1, as published by the Free Software Foundation.
 *
 * This program is distributed in the hope it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public License for
 * more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin St - Fifth Floor, Boston, MA 02110-1301 USA.
 *
 */

#ifndef __MN_STYLE_H__
#define __MN_STYLE_H__

#include <glib-object.h>

G_BEGIN_DECLS

#define MN_TYPE_STYLE                 (mn_style_get_type ())
#define MN_STYLE(obj)                 (G_TYPE_CHECK_INSTANCE_CAST ((obj), MN_TYPE_STYLE, MnStyle))
#define MN_IS_STYLE(obj)              (G_TYPE_CHECK_INSTANCE_TYPE ((obj), MN_TYPE_STYLE))
#define MN_STYLE_CLASS(klass)         (G_TYPE_CHECK_CLASS_CAST ((klass), MN_TYPE_STYLE, MnStyleClass))
#define MN_IS_STYLE_CLASS(klass)      (G_TYPE_CHECK_CLASS_TYPE ((klass), MN_TYPE_STYLE))
#define MN_STYLE_GET_CLASS(obj)       (G_TYPE_INSTANCE_GET_CLASS ((obj), MN_TYPE_STYLE, MnStyleClass))

typedef struct _MnStyle               MnStyle;
typedef struct _MnStylePrivate        MnStylePrivate;
typedef struct _MnStyleClass          MnStyleClass;

/* forward declaration */
typedef struct _MnStylable            MnStylable; /* dummy typedef */
typedef struct _MnStylableIface       MnStylableIface;

typedef enum { /*< prefix=MN_STYLE_ERROR >*/
  MN_STYLE_ERROR_INVALID_FILE
} MnStyleError;

/**
 * MnStyle:
 *
 * The contents of this structure is private and should only be accessed using
 * the provided API.
 */
struct _MnStyle
{
  /*< private >*/
  GObject parent_instance;

  MnStylePrivate *priv;
};

struct _MnStyleClass
{
  GObjectClass parent_class;

  void (* changed) (MnStyle *style);
};

GType            mn_style_get_type     (void) G_GNUC_CONST;

MnStyle *      mn_style_get_default  (void);
MnStyle *      mn_style_new          (void);

gboolean         mn_style_load_from_file (MnStyle     *style,
                                            const gchar   *filename,
                                            GError       **error);
void             mn_style_get_property   (MnStyle     *style,
                                            MnStylable  *stylable,
                                            GParamSpec    *pspec,
                                            GValue        *value);
void             mn_style_get            (MnStyle     *style,
                                            MnStylable  *stylable,
                                            const gchar   *first_property_name,
                                            ...) G_GNUC_NULL_TERMINATED;
void             mn_style_get_valist     (MnStyle     *style,
                                            MnStylable  *stylable,
                                            const gchar   *first_property_name,
                                            va_list        va_args);

G_END_DECLS

#endif /* __MN_STYLE_H__ */
