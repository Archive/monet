/*
 * mn-stylable.c: Interface for stylable objects
 *
 * Copyright 2008 Intel Corporation
 * Copyright 2009 Intel Corporation.
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms and conditions of the GNU Lesser General Public License,
 * version 2.1, as published by the Free Software Foundation.
 *
 * This program is distributed in the hope it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public License for
 * more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin St - Fifth Floor, Boston, MA 02110-1301 USA.
 *
 * Written by: Emmanuele Bassi <ebassi@openedhand.com>
 *             Thomas Wood <thomas@linux.intel.com>
 *
 */

/**
 * SECTION:mn-stylable
 * @short_description: Interface for stylable objects
 *
 * Stylable objects are classes that can have "style properties", that is
 * properties that can be changed by attaching a #MnStyle to them.
 */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <stdlib.h>
#include <string.h>

#include <glib-object.h>
#include <gobject/gvaluecollector.h>
#include <gobject/gobjectnotifyqueue.c>

#include "mn-marshal.h"
#include "mn-stylable.h"
#include "mn-private.h"

enum
{
  STYLE_CHANGED,
  STYLE_NOTIFY,
  CHANGED,

  LAST_SIGNAL
};

static GObjectNotifyContext property_notify_context = { 0, };

static GParamSpecPool *style_property_spec_pool = NULL;

static GQuark          quark_real_owner         = 0;
static GQuark          quark_style              = 0;

static guint stylable_signals[LAST_SIGNAL] = { 0, };

static void
mn_stylable_notify_dispatcher (GObject     *gobject,
                                 guint        n_pspecs,
                                 GParamSpec **pspecs)
{
  guint i;

  for (i = 0; i < n_pspecs; i++)
    g_signal_emit (gobject, stylable_signals[STYLE_NOTIFY],
                   g_quark_from_string (pspecs[i]->name),
                   pspecs[i]);
}

static void
mn_stylable_base_finalize (gpointer g_iface)
{
  GList *list, *node;

  list = g_param_spec_pool_list_owned (style_property_spec_pool,
                                       G_TYPE_FROM_INTERFACE (g_iface));

  for (node = list; node; node = node->next)
    {
      GParamSpec *pspec = node->data;

      g_param_spec_pool_remove (style_property_spec_pool, pspec);
      g_param_spec_unref (pspec);
    }

  g_list_free (list);
}

static void
mn_stylable_base_init (gpointer g_iface)
{
  static gboolean initialised = FALSE;
  GParamSpec *pspec;
  GType iface_type = G_TYPE_FROM_INTERFACE (g_iface);

  if (G_LIKELY (initialised))
    return;

  initialised = TRUE;

  quark_real_owner =
    g_quark_from_static_string ("mn-stylable-real-owner-quark");
  quark_style = g_quark_from_static_string ("mn-stylable-style-quark");

  style_property_spec_pool = g_param_spec_pool_new (FALSE);

  property_notify_context.quark_notify_queue =
    g_quark_from_static_string ("MnStylable-style-property-notify-queue");
  property_notify_context.dispatcher = mn_stylable_notify_dispatcher;

  pspec = g_param_spec_object ("style",
                               "Style",
                               "A style object",
                               MN_TYPE_STYLE,
                               MN_PARAM_READWRITE);
  g_object_interface_install_property (g_iface, pspec);

  pspec = g_param_spec_string ("style-type",
                               "Style Type",
                               "String representation of the item's type",
                               "",
                               MN_PARAM_READWRITE);
  g_object_interface_install_property (g_iface, pspec);

  pspec = g_param_spec_string ("style-class",
                               "Style Class",
                               "String representation of the item's class",
                               "",
                               MN_PARAM_READWRITE);
  g_object_interface_install_property (g_iface, pspec);

  pspec = g_param_spec_string ("style-id",
                               "Style ID",
                               "String representation of the item's name",
                               "",
                               MN_PARAM_READWRITE);
  g_object_interface_install_property (g_iface, pspec);

  pspec = g_param_spec_string ("style-pseudo-class",
                               "Style Pseudo Class",
                               "Pseudo class, such as current state",
                               "",
                               MN_PARAM_READWRITE);
  g_object_interface_install_property (g_iface, pspec);

  /**
   * MnStylable::style-changed:
   * @stylable: the #MnStylable that received the signal
   * @old_style: the previously set #MnStyle for @stylable
   *
   * The ::style-changed signal is emitted each time one of the style
   * properties have changed.
   */
  stylable_signals[STYLE_CHANGED] =
    g_signal_new ("style-changed",
                  iface_type,
                  G_SIGNAL_RUN_FIRST,
                  G_STRUCT_OFFSET (MnStylableIface, style_changed),
                  NULL, NULL,
                  _mn_marshal_VOID__VOID,
                  G_TYPE_NONE, 0);

  /**
   * MnStylable::stylable-changed:
   * @actor: the actor that received the signal
   *
   * The ::changed signal is emitted each time any of the properties of the
   * stylable has changed.
   */
  stylable_signals[CHANGED] =
    g_signal_new ("stylable-changed",
                  iface_type,
                  G_SIGNAL_RUN_LAST,
                  G_STRUCT_OFFSET (MnStylableIface, stylable_changed),
                  NULL, NULL,
                  _mn_marshal_VOID__VOID,
                  G_TYPE_NONE, 0);

  stylable_signals[STYLE_NOTIFY] =
    g_signal_new ("style-notify",
                  iface_type,
                  G_SIGNAL_RUN_FIRST | G_SIGNAL_NO_RECURSE | G_SIGNAL_DETAILED | G_SIGNAL_NO_HOOKS | G_SIGNAL_ACTION,
                  G_STRUCT_OFFSET (MnStylableIface, style_notify),
                  NULL, NULL,
                  _mn_marshal_VOID__PARAM,
                  G_TYPE_NONE, 1,
                  G_TYPE_PARAM);
}

GType
mn_stylable_get_type (void)
{
  static GType our_type = 0;

  if (G_UNLIKELY (our_type == 0))
    {
      GTypeInfo stylable_info = {
        sizeof (MnStylableIface),
        mn_stylable_base_init,
        mn_stylable_base_finalize
      };

      our_type = g_type_register_static (G_TYPE_INTERFACE,
                                         "MnStylable",
                                         &stylable_info, 0);
    }

  return our_type;
}

void
mn_stylable_freeze_notify (MnStylable *stylable)
{
  g_return_if_fail (MN_IS_STYLABLE (stylable));

  g_object_ref (stylable);
  g_object_notify_queue_freeze (G_OBJECT (stylable), &property_notify_context);
  g_object_unref (stylable);
}

void
mn_stylable_thaw_notify (MnStylable *stylable)
{
  GObjectNotifyQueue *nqueue;

  g_return_if_fail (MN_IS_STYLABLE (stylable));

  g_object_ref (stylable);

  nqueue = g_object_notify_queue_from_object (G_OBJECT (stylable),
                                              &property_notify_context);

  if (!nqueue || !nqueue->freeze_count)
    g_warning ("%s: property-changed notification for %s(%p) is not frozen",
               G_STRFUNC, G_OBJECT_TYPE_NAME (stylable), stylable);
  else
    g_object_notify_queue_thaw (G_OBJECT (stylable), nqueue);

  g_object_unref (stylable);
}

void
mn_stylable_notify (MnStylable *stylable,
                      const gchar  *property_name)
{
  GParamSpec *pspec;
    
  g_return_if_fail (MN_IS_STYLABLE (stylable));
  g_return_if_fail (property_name != NULL);

  g_object_ref (stylable);

  pspec = g_param_spec_pool_lookup (style_property_spec_pool,
                                    property_name,
                                    G_OBJECT_TYPE (stylable),
                                    TRUE);

  if (!pspec)
    g_warning ("%s: object class `%s' has no style property named `%s'",
               G_STRFUNC,
               G_OBJECT_TYPE_NAME (stylable),
               property_name);
  else
    {
      GObjectNotifyQueue *nqueue;
      
      nqueue = g_object_notify_queue_freeze (G_OBJECT (stylable),
                                              &property_notify_context);
      g_object_notify_queue_add (G_OBJECT (stylable), nqueue, pspec);
      g_object_notify_queue_thaw (G_OBJECT (stylable), nqueue);
    }

  g_object_unref (stylable);
}

/**
 * mn_stylable_iface_install_property:
 * @iface: a #MnStylableIface
 * @owner_type: #GType of the style property owner
 * @pspec: a #GParamSpec
 *
 * Installs a property for @owner_type using @pspec as the property
 * description.
 *
 * This function should be used inside the #MnStylableIface initialization
 * function of a class, for instance:
 *
 * <informalexample><programlisting>
 * G_DEFINE_TYPE_WITH_CODE (FooActor, foo_actor, CLUTTER_TYPE_ACTOR,
 *                          G_IMPLEMENT_INTERFACE (MN_TYPE_STYLABLE,
 *                                                 mn_stylable_init));
 * ...
 * static void
 * mn_stylable_init (MnStylableIface *iface)
 * {
 *   static gboolean is_initialized = FALSE;
 *
 *   if (!is_initialized)
 *     {
 *       ...
 *       mn_stylable_iface_install_property (stylable,
 *                                             FOO_TYPE_ACTOR,
 *                                             g_param_spec_int ("x-spacing",
 *                                                               "X Spacing",
 *                                                               "Horizontal spacing",
 *                                                               -1, G_MAXINT,
 *                                                               2,
 *                                                               G_PARAM_READWRITE));
 *       ...
 *     }
 * }
 * </programlisting></informalexample>
 */
void
mn_stylable_iface_install_property (MnStylableIface *iface,
                                      GType              owner_type,
                                      GParamSpec        *pspec)
{
  g_return_if_fail (MN_IS_STYLABLE_IFACE (iface));
  g_return_if_fail (owner_type != G_TYPE_INVALID);
  g_return_if_fail (G_IS_PARAM_SPEC (pspec));
  g_return_if_fail (pspec->flags & G_PARAM_READABLE);
  g_return_if_fail (!(pspec->flags & (G_PARAM_CONSTRUCT_ONLY | G_PARAM_CONSTRUCT
)));

  if (g_param_spec_pool_lookup (style_property_spec_pool, pspec->name,
                                owner_type,
                                FALSE))
    {
      g_warning ("%s: class `%s' already contains a style property named `%s'",
                 G_STRLOC,
                 g_type_name (owner_type),
                 pspec->name);
      return;
    }

  g_param_spec_ref_sink (pspec);
  g_param_spec_set_qdata_full (pspec, quark_real_owner,
                               g_strdup (g_type_name (owner_type)),
                               g_free);

  g_param_spec_pool_insert (style_property_spec_pool,
                            pspec,
                            owner_type);
}

/**
 * mn_stylable_list_properties:
 * @stylable: a #MnStylable
 * @n_props: return location for the number of properties, or %NULL
 *
 * Retrieves all the #GParamSpec<!-- -->s installed by @stylable.
 *
 * Return value: an array of #GParamSpec<!-- -->s. Free it with
 *   g_free() when done.
 */
GParamSpec **
mn_stylable_list_properties (MnStylable *stylable,
                               guint        *n_props)
{
  GParamSpec **pspecs = NULL;
  guint n;

  g_return_val_if_fail (MN_IS_STYLABLE (stylable), NULL);

  pspecs = g_param_spec_pool_list (style_property_spec_pool,
                                   G_OBJECT_TYPE (stylable),
                                   &n);
  if (n_props)
    *n_props = n;

  return pspecs;
}

/**
 * mn_stylable_find_property:
 * @stylable: a #MnStylable
 * @property_name: the name of the property to find
 *
 * Finds the #GParamSpec installed by @stylable for the property
 * with @property_name.
 *
 * Return value: a #GParamSpec for the given property, or %NULL if
 *   no property with that name was found
 */
GParamSpec *
mn_stylable_find_property (MnStylable *stylable,
                             const gchar  *property_name)
{
  g_return_val_if_fail (MN_IS_STYLABLE (stylable), NULL);
  g_return_val_if_fail (property_name != NULL, NULL);

  return g_param_spec_pool_lookup (style_property_spec_pool,
                                   property_name,
                                   G_OBJECT_TYPE (stylable),
                                   TRUE);
}

static inline void
mn_stylable_get_property_internal (MnStylable *stylable,
                                     GParamSpec   *pspec,
                                     GValue       *value)
{
  MnStyle *style;
  GValue real_value = { 0, };

  style = mn_stylable_get_style (stylable);

  if (!style)
    {
      g_value_reset (value);
      return;
    }

  mn_style_get_property (style, stylable, pspec, &real_value);

  g_value_copy (&real_value, value);
  g_value_unset (&real_value);

}

/**
 * mn_stylable_get_property:
 * @stylable: a #MnStylable
 * @property_name: the name of the property
 * @value: return location for an empty #GValue
 *
 * Retrieves the value of @property_name for @stylable, and puts it
 * into @value.
 */
void
mn_stylable_get_property (MnStylable *stylable,
                            const gchar  *property_name,
                            GValue       *value)
{
  GParamSpec *pspec;

  g_return_if_fail (MN_IS_STYLABLE (stylable));
  g_return_if_fail (property_name != NULL);
  g_return_if_fail (value != NULL);

  pspec = mn_stylable_find_property (stylable, property_name);
  if (!pspec)
    {
      g_warning ("Stylable class `%s' doesn't have a property named `%s'",
                 g_type_name (G_OBJECT_TYPE (stylable)),
                 property_name);
      return;
    }

  if (!(pspec->flags & G_PARAM_READABLE))
    {
      g_warning ("Style property `%s' of class `%s' is not readable",
                 pspec->name,
                 g_type_name (G_OBJECT_TYPE (stylable)));
      return;
    }

  if (G_VALUE_TYPE (value) != G_PARAM_SPEC_VALUE_TYPE (pspec))
    {
      g_warning ("Passed value is not of the requested type `%s' for "
                 "the style property `%s' of class `%s'",
                 g_type_name (G_PARAM_SPEC_VALUE_TYPE (pspec)),
                 pspec->name,
                 g_type_name (G_OBJECT_TYPE (stylable)));
      return;
    }

  mn_stylable_get_property_internal (stylable, pspec, value);
}

/**
 * mn_stylable_get:
 * @stylable: a #MnStylable
 * @first_property_name: name of the first property to get
 * @Varargs: return location for the first property, followed optionally
 *   by more name/return location pairs, followed by %NULL
 *
 * Gets the style properties for @stylable.
 *
 * In general, a copy is made of the property contents and the called
 * is responsible for freeing the memory in the appropriate manner for
 * the property type.
 *
 * <example>
 * <title>Using mn_stylable_get(<!-- -->)</title>
 * <para>An example of using mn_stylable_get() to get the contents of
 * two style properties - one of type #G_TYPE_INT and one of type
 * #CLUTTER_TYPE_COLOR:</para>
 * <programlisting>
 *   gint x_spacing;
 *   ClutterColor *bg_color;
 *
 *   mn_stylable_get (stylable,
 *                      "x-spacing", &amp;x_spacing,
 *                      "bg-color", &amp;bg_color,
 *                      NULL);
 *
 *   /<!-- -->* do something with x_spacing and bg_color *<!-- -->/
 *
 *   clutter_color_free (bg_color);
 * </programlisting>
 * </example>
 */
void
mn_stylable_get (MnStylable *stylable,
                   const gchar  *first_property_name,
                                 ...)
{
  MnStyle *style;
  va_list args;

  g_return_if_fail (MN_IS_STYLABLE (stylable));
  g_return_if_fail (first_property_name != NULL);

  style = mn_stylable_get_style (stylable);

  va_start (args, first_property_name);
  mn_style_get_valist (style, stylable, first_property_name, args);
  va_end (args);
}

/**
 * mn_stylable_get_default_value:
 * @stylable: a #MnStylable
 * @property_name: name of the property to query
 * @value_out: return location for the default value
 *
 * Query @stylable for the default value of property @property_name and
 * fill @value_out with the result.
 *
 * Returns: %TRUE if property @property_name exists and the default value has
 * been returned.
 */
gboolean
mn_stylable_get_default_value (MnStylable  *stylable,
                                 const gchar   *property_name,
                                 GValue        *value_out)
{
  GParamSpec *pspec;

  pspec = mn_stylable_find_property (stylable, property_name);
  if (!pspec)
    {
      g_warning ("%s: no style property named `%s' found for class `%s'",
                  G_STRLOC,
                  property_name,
                  g_type_name (G_OBJECT_TYPE (stylable)));
      return FALSE;
    }

  if (!(pspec->flags & G_PARAM_READABLE))
    {
      g_warning ("Style property `%s' of class `%s' is not readable",
                  pspec->name,
                  g_type_name (G_OBJECT_TYPE (stylable)));
      return FALSE;
    }

  g_value_init (value_out, G_PARAM_SPEC_VALUE_TYPE (pspec));
  g_param_value_set_default (pspec, value_out);
  return TRUE;
}

/**
 * mn_stylable_get_style:
 * @stylable: a #MnStylable
 *
 * Retrieves the #MnStyle used by @stylable. This function does not
 * alter the reference count of the returned object.
 *
 * Return value: a #MnStyle
 */
MnStyle *
mn_stylable_get_style (MnStylable *stylable)
{
  MnStylableIface *iface;

  g_return_val_if_fail (MN_IS_STYLABLE (stylable), NULL);

  iface = MN_STYLABLE_GET_IFACE (stylable);
  if (iface->get_style)
    return iface->get_style (stylable);

  return g_object_get_data (G_OBJECT (stylable), "mn-stylable-style");
}

/**
 * mn_stylable_set_style:
 * @stylable: a #MnStylable
 * @style: a #MnStyle
 *
 * Sets @style as the new #MnStyle to be used by @stylable.
 *
 * The #MnStylable will take ownership of the passed #MnStyle.
 *
 * After the #MnStle has been set, the MnStylable::style-set signal
 * will be emitted.
 */
void
mn_stylable_set_style (MnStylable *stylable,
                         MnStyle    *style)
{
  MnStylableIface *iface;
  MnStyle *old_style;

  g_return_if_fail (MN_IS_STYLABLE (stylable));
  g_return_if_fail (MN_IS_STYLE (style));

  iface = MN_STYLABLE_GET_IFACE (stylable);

  old_style = mn_stylable_get_style (stylable);
  g_object_ref (old_style);

  if (iface->set_style)
    iface->set_style (stylable, style);
  else
    {
      g_object_set_qdata_full (G_OBJECT (stylable),
                               quark_style,
                               g_object_ref_sink (style),
                               g_object_unref);
    }

  g_signal_emit (stylable, stylable_signals[STYLE_CHANGED], 0, old_style);
  g_object_unref (old_style);

  g_object_notify (G_OBJECT (stylable), "style");
}

/**
 * mn_stylable_changed:
 * @stylable: A #MnStylable
 *
 * Emit the "stylable-changed" signal on @stylable
 */
void
mn_stylable_changed (MnStylable *stylable)
{
  g_signal_emit (stylable, stylable_signals[CHANGED], 0, NULL);
}
