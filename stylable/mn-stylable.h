/*
 * mn-stylable.h: Interface for stylable objects
 *
 * Copyright 2008, 2009 Intel Corporation
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms and conditions of the GNU Lesser General Public License,
 * version 2.1, as published by the Free Software Foundation.
 *
 * This program is distributed in the hope it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public License for
 * more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin St - Fifth Floor, Boston, MA 02110-1301 USA.
 * Boston, MA 02111-1307, USA.
 *
 * Written by: Emmanuele Bassi <ebassi@openedhand.com>
 *             Thomas Wood <thomas@linux.intel.com>
 *
 */

#ifndef __MN_STYLABLE_H__
#define __MN_STYLABLE_H__

#include <glib-object.h>
#include <monet/mn-style.h>

G_BEGIN_DECLS

#define MN_TYPE_STYLABLE              (mn_stylable_get_type ())
#define MN_STYLABLE(obj)              (G_TYPE_CHECK_INSTANCE_CAST ((obj), MN_TYPE_STYLABLE, MnStylable))
#define MN_IS_STYLABLE(obj)           (G_TYPE_CHECK_INSTANCE_TYPE ((obj), MN_TYPE_STYLABLE))
#define MN_STYLABLE_IFACE(iface)      (G_TYPE_CHECK_CLASS_CAST ((iface), MN_TYPE_STYLABLE, MnStylableIface))
#define MN_IS_STYLABLE_IFACE(iface)   (G_TYPE_CHECK_CLASS_TYPE ((iface), MN_TYPE_STYLABLE))
#define MN_STYLABLE_GET_IFACE(obj)    (G_TYPE_INSTANCE_GET_INTERFACE ((obj), MN_TYPE_STYLABLE, MnStylableIface))

/* MnStylableIface is defined in mn-style.h */

struct _MnStylableIface
{
  GTypeInterface g_iface;

  /* virtual functions */
  MnStyle *(* get_style) (MnStylable *stylable);
  void     (* set_style) (MnStylable *stylable,
                          MnStyle    *style);

  /* context virtual functions */

  /* signals, not vfuncs */
  void (* style_notify)     (MnStylable *stylable,
                             GParamSpec   *pspec);
  void (* style_changed)    (MnStylable *stylable);

  void (* stylable_changed) (MnStylable *stylable);
};

GType        mn_stylable_get_type               (void) G_GNUC_CONST;

void         mn_stylable_iface_install_property (MnStylableIface *iface,
                                                 GType              owner_type,
                                                 GParamSpec        *pspec);

void         mn_stylable_freeze_notify          (MnStylable      *stylable);
void         mn_stylable_notify                 (MnStylable      *stylable,
                                                 const gchar       *property_name);
void         mn_stylable_thaw_notify            (MnStylable      *stylable);
GParamSpec **mn_stylable_list_properties        (MnStylable      *stylable,
                                                 guint             *n_props);
GParamSpec * mn_stylable_find_property          (MnStylable      *stylable,
                                                 const gchar       *property_name);
void         mn_stylable_set_style              (MnStylable      *stylable,
                                                 MnStyle         *style);
MnStyle *    mn_stylable_get_style              (MnStylable      *stylable);

void         mn_stylable_get                    (MnStylable      *stylable,
                                                 const gchar       *first_property_name,
                                                 ...) G_GNUC_NULL_TERMINATED;
void         mn_stylable_get_property           (MnStylable      *stylable,
                                                 const gchar       *property_name,
                                                 GValue            *value);
gboolean     mn_stylable_get_default_value      (MnStylable      *stylable,
                                                 const gchar       *property_name,
                                                 GValue            *value_out);


void mn_stylable_changed (MnStylable *stylable);
G_END_DECLS

#endif /* __MN_STYLABLE_H__ */
