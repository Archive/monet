#include <monet/monet.h>
#include <stdio.h>

int
main (int argc, char **argv)
{
  MnStyle *style;
  MnItem *item1, *item2;
  MnColor *color;
  gchar *string;
  GError *err = NULL;

  monet_init (&argc, &argv);

  style = mn_style_new ();
  mn_style_load_from_file (style, "test-stylable.css", &err);

  if (err)
    {
      printf ("Error: %s\n", err->message);
      return 1;
    }

  item1 = mn_item_new (style, "TestItem", NULL, "item-one", NULL);

  item2 = mn_item_new (style, "TestItem", NULL, "item-two", NULL);

  mn_stylable_get (MN_STYLABLE (item1), "background-color", &color, NULL);
  string = mn_color_to_string (color);
  printf ("item1 background = %s\n", string);
  g_free (string);

  mn_stylable_get (MN_STYLABLE (item2), "background-color", &color, NULL);
  string = mn_color_to_string (color);
  printf ("item2 background = %s\n", string);
  g_free (string);

  g_object_unref (item1);
  g_object_unref (item2);
  g_object_unref (style);

  return 0;
}
