/*
 * mn-button.h: parameters required for drawing buttons
 *
 * Copyright 2009 Intel Corporation
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301
 * USA
 *
 * Author: Thomas Wood <thos@gnome.org>
 */

#ifndef _MN_BUTTON_H
#define _MN_BUTTON_H

#include <glib-object.h>
#include <monet/mn-widget.h>

G_BEGIN_DECLS

#define MN_TYPE_BUTTON mn_button_get_type()

#define MN_BUTTON(obj) \
  (G_TYPE_CHECK_INSTANCE_CAST ((obj), \
  MN_TYPE_BUTTON, MnButton))

#define MN_BUTTON_CLASS(klass) \
  (G_TYPE_CHECK_CLASS_CAST ((klass), \
  MN_TYPE_BUTTON, MnButtonClass))

#define MN_IS_BUTTON(obj) \
  (G_TYPE_CHECK_INSTANCE_TYPE ((obj), \
  MN_TYPE_BUTTON))

#define MN_IS_BUTTON_CLASS(klass) \
  (G_TYPE_CHECK_CLASS_TYPE ((klass), \
  MN_TYPE_BUTTON))

#define MN_BUTTON_GET_CLASS(obj) \
  (G_TYPE_INSTANCE_GET_CLASS ((obj), \
  MN_TYPE_BUTTON, MnButtonClass))

typedef struct _MnButton MnButton;
typedef struct _MnButtonClass MnButtonClass;
typedef struct _MnButtonPrivate MnButtonPrivate;

struct _MnButton
{
  MnWidget parent;

  MnButtonPrivate *priv;
};

struct _MnButtonClass
{
  MnWidgetClass parent_class;
};

GType mn_button_get_type (void) G_GNUC_CONST;

MnButton *mn_button_new (void);

G_END_DECLS

#endif /* _MN_BUTTON_H */
