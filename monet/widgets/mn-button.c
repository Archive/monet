/*
 * mn-button.c: parameters required for drawing buttons
 *
 * Copyright 2009 Intel Corporation
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301
 * USA
 *
 * Author: Thomas Wood <thos@gnome.org>
 */

#include "mn-button.h"

G_DEFINE_TYPE (MnButton, mn_button, MN_TYPE_WIDGET)

#define BUTTON_PRIVATE(o) \
  (G_TYPE_INSTANCE_GET_PRIVATE ((o), MN_TYPE_BUTTON, MnButtonPrivate))

struct _MnButtonPrivate
{
  gboolean has_default;
};

enum
{
  PROP_HAS_DEFAULT
};

static void
mn_button_get_property (GObject *object, guint property_id, GValue *value, GParamSpec *pspec)
{
  MnButtonPrivate *priv = MN_BUTTON (object)->priv;

  switch (property_id)
    {
    case PROP_HAS_DEFAULT:
      g_value_set_boolean (value, priv->has_default);
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
    }
}

static void
mn_button_set_property (GObject *object, guint property_id, const GValue *value, GParamSpec *pspec)
{
  MnButtonPrivate *priv = MN_BUTTON (object)->priv;

  switch (property_id)
    {
    case PROP_HAS_DEFAULT:
      priv->has_default = g_value_get_boolean (value);
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
    }
}

static void
mn_button_class_init (MnButtonClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);

  g_type_class_add_private (klass, sizeof (MnButtonPrivate));

  object_class->get_property = mn_button_get_property;
  object_class->set_property = mn_button_set_property;
}

static void
mn_button_init (MnButton *self)
{
  self->priv = BUTTON_PRIVATE (self);
}

MnButton *
mn_button_new (void)
{
  return g_object_new (MN_TYPE_BUTTON, NULL);
}
