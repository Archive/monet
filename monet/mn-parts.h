/*
 * mn-parts.h: part definitions for complex widgets
 *
 * Copyright 2009 Intel Corporation
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301
 * USA
 *
 * Author: Thomas Wood <thos@gnome.org>
 */

typedef enum
{
  MN_SCROLLBAR_STEPPER_UP,
  MN_SCROLLBAR_STEPPER_DOWN,
  MN_SCROLLBAR_STEPPER_LEFT,
  MN_SCROLLBAR_STEPPER_RIGHT,
  MN_SCROLLBAR_TROUGH,
  MN_SCROLLBAR_SLIDER
} MnScrollbarParts;

typedef enum
{
  MN_COMBOBOX_ENTRY,
  MN_COMBOBOX_BUTTON
} MnComboboxParts;

typedef enum
{
  MN_SLIDER_TROUGH,
  MN_SLIDER_HANDLE
} MnSliderParts;

typedef enum
{
  MN_TOOLBAR_GRIP,
  MN_TOOLBAR_BACKGROUND,
  MN_TOOLBAR_SEPERATOR,
  MN_TOOLBAR_BUTTON
} MnToolbarParts;

typedef enum
{
  MN_PROGRESSBAR_BACKGROUND,
  MN_PROGRESSBAR_INDICATOR
} MnProgressbarParts;

typedef enum
{
  MN_SPIN_BUTTON_ENTRY,
  MN_SPIN_BUTTON_STEPPER_UP,
  MN_SPIN_BUTTON_STEPPER_DOWN
} MnSpinbuttonParts;

typedef enum
{
  MN_TREEVIEW_COLUMN_HEADER,
  MN_TREEVIEW_BORDER,
  MN_TREEVIEW_SELECTION
} MnTreeviewParts;

typedef enum
{
  MN_NOTEBOOK_TAB,
  MN_NOTEBOOK_BORDER
} MnNotebookParts;
