/* mn-theme-engine.c */

#include "mn-theme-engine.h"

G_DEFINE_ABSTRACT_TYPE (MnThemeEngine, mn_theme_engine, G_TYPE_OBJECT)

#define THEME_ENGINE_PRIVATE(o) \
  (G_TYPE_INSTANCE_GET_PRIVATE ((o), MN_TYPE_THEME_ENGINE, MnThemeEnginePrivate))

struct _MnThemeEnginePrivate
{
  gpointer dummy;
};


static void
mn_theme_engine_get_property (GObject *object, guint property_id, GValue *value, GParamSpec *pspec)
{
  switch (property_id)
    {
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
    }
}

static void
mn_theme_engine_set_property (GObject *object, guint property_id, const GValue *value, GParamSpec *pspec)
{
  switch (property_id)
    {
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
    }
}

static void
mn_theme_engine_dispose (GObject *object)
{
  G_OBJECT_CLASS (mn_theme_engine_parent_class)->dispose (object);
}

static void
mn_theme_engine_finalize (GObject *object)
{
  G_OBJECT_CLASS (mn_theme_engine_parent_class)->finalize (object);
}

static void
mn_theme_engine_class_init (MnThemeEngineClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);

  g_type_class_add_private (klass, sizeof (MnThemeEnginePrivate));

  object_class->get_property = mn_theme_engine_get_property;
  object_class->set_property = mn_theme_engine_set_property;
  object_class->dispose = mn_theme_engine_dispose;
  object_class->finalize = mn_theme_engine_finalize;
}

static void
mn_theme_engine_init (MnThemeEngine *self)
{
  self->priv = THEME_ENGINE_PRIVATE (self);
}

gboolean
mn_theme_engine_paint_widget (MnThemeEngine *engine,
                              MnWidget      *widget,
                              cairo_t       *cr)
{
  MnThemeEngineClass *klass = MN_THEME_ENGINE_GET_CLASS (engine);

  if (klass->paint_widget)
    return klass->paint_widget (engine, widget, cr);
  else
    g_warning ("Theme Engine of type '%s' does not implement the"
               " required MnThemeEngine::%s virtual function.",
               G_OBJECT_TYPE_NAME (engine), "paint_widget");

  return FALSE;
}

