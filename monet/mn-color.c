/*
 * MnColour
 *
 * Based on clutter-color.c: Copyright (C) 2006 OpenedHand
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

/**
 * SECTION:mn-color
 * @short_description: Color management and manipulation.
 *
 * #MnColor is a simple type for representing colors in Mn.
 *
 * A #MnColor is expressed as a 4-tuple of values ranging from
 * zero to 255, one for each color channel plus one for the alpha.
 */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <gobject/gvaluecollector.h>
#include <stdio.h>
#include <string.h>

#include "mn-color.h"

/**
 * mn_color_add:
 * @a: a #MnColor
 * @b: a #MnColor
 * @result: (out): return location for the result
 *
 * Adds @a to @b and saves the resulting color inside @result.
 *
 * The alpha channel of @result is set as as the maximum value
 * between the alpha channels of @a and @b.
 */
void
mn_color_add (const MnColor *a,
		   const MnColor *b,
		   MnColor       *result)
{
  g_return_if_fail (a != NULL);
  g_return_if_fail (b != NULL);
  g_return_if_fail (result != NULL);

  result->red   = CLAMP (a->red   + b->red,   0, 255);
  result->green = CLAMP (a->green + b->green, 0, 255);
  result->blue  = CLAMP (a->blue  + b->blue,  0, 255);

  result->alpha = MAX (a->alpha, b->alpha);
}

/**
 * mn_color_subtract:
 * @a: a #MnColor
 * @b: a #MnColor
 * @result: (out): return location for the result
 *
 * Subtracts @b from @a and saves the resulting color inside @result.
 *
 * This function assumes that the components of @a are greater than the
 * components of @b; the result is, otherwise, undefined.
 *
 * The alpha channel of @result is set as the minimum value
 * between the alpha channels of @a and @b.
 */
void
mn_color_subtract (const MnColor *a,
			const MnColor *b,
			MnColor       *result)
{
  g_return_if_fail (a != NULL);
  g_return_if_fail (b != NULL);
  g_return_if_fail (result != NULL);

  result->red   = CLAMP (a->red   - b->red,   0, 255);
  result->green = CLAMP (a->green - b->green, 0, 255);
  result->blue  = CLAMP (a->blue  - b->blue,  0, 255);

  result->alpha = MIN (a->alpha, b->alpha);
}

/**
 * mn_color_lighten:
 * @color: a #MnColor
 * @result: (out): return location for the lighter color
 *
 * Lightens @color by a fixed amount, and saves the changed color
 * in @result.
 */
void
mn_color_lighten (const MnColor *color,
		       MnColor       *result)
{
  mn_color_shade (color, 1.3, result);
}

/**
 * mn_color_darken:
 * @color: a #MnColor
 * @result: (out): return location for the darker color
 *
 * Darkens @color by a fixed amount, and saves the changed color
 * in @result.
 */
void
mn_color_darken (const MnColor *color,
		      MnColor       *result)
{
  mn_color_shade (color, 0.7, result);
}

/**
 * mn_color_to_hls:
 * @color: a #MnColor
 * @hue: return location for the hue value or %NULL
 * @luminance: return location for the luminance value or %NULL
 * @saturation: return location for the saturation value or %NULL
 *
 * Converts @color to the HLS format.
 *
 * The @hue value is in the 0 .. 360 range. The @luminance and
 * @saturation values are in the 0 .. 1 range.
 */
void
mn_color_to_hls (const MnColor *color,
		      float              *hue,
		      float              *luminance,
		      float              *saturation)
{
  float red, green, blue;
  float min, max, delta;
  float h, l, s;
  
  g_return_if_fail (color != NULL);

  red   = color->red / 255.0;
  green = color->green / 255.0;
  blue  = color->blue / 255.0;

  if (red > green)
    {
      if (red > blue)
	max = red;
      else
	max = blue;

      if (green < blue)
	min = green;
      else
	min = blue;
    }
  else
    {
      if (green > blue)
	max = green;
      else
	max = blue;

      if (red < blue)
	min = red;
      else
	min = blue;
    }

  l = (max + min) / 2;
  s = 0;
  h = 0;

  if (max != min)
    {
      if (l <= 0.5)
	s = (max - min) / (max + min);
      else
	s = (max - min) / (2.0 - max - min);

      delta = max - min;

      if (red == max)
	h = (green - blue) / delta;
      else if (green == max)
	h = 2.0 + (blue - red) / delta;
      else if (blue == max)
	h = 4.0 + (red - green) / delta;

      h *= 60;

      if (h < 0)
	h += 360.0;
    }

  if (hue)
    *hue = h;

  if (luminance)
    *luminance = l;

  if (saturation)
    *saturation = s;
}

/**
 * mn_color_from_hls:
 * @color: (out): return location for a #MnColor
 * @hue: hue value, in the 0 .. 360 range
 * @luminance: luminance value, in the 0 .. 1 range
 * @saturation: saturation value, in the 0 .. 1 range
 *
 * Converts a color expressed in HLS (hue, luminance and saturation)
 * values into a #MnColor.
 */
void
mn_color_from_hls (MnColor *color,
			float         hue,
			float         luminance,
			float         saturation)
{
  float tmp1, tmp2;
  float tmp3[3];
  float clr[3];
  int   i;

  hue /= 360.0;

  if (saturation == 0)
    {
      color->red = color->green = color->blue = (luminance * 255);

      return;
    }

  if (luminance <= 0.5)
    tmp2 = luminance * (1.0 + saturation);
  else
    tmp2 = luminance + saturation - (luminance * saturation);

  tmp1 = 2.0 * luminance - tmp2;

  tmp3[0] = hue + 1.0 / 3.0;
  tmp3[1] = hue;
  tmp3[2] = hue - 1.0 / 3.0;

  for (i = 0; i < 3; i++)
    {
      if (tmp3[i] < 0)
        tmp3[i] += 1.0;

      if (tmp3[i] > 1)
        tmp3[i] -= 1.0;

      if (6.0 * tmp3[i] < 1.0)
        clr[i] = tmp1 + (tmp2 - tmp1) * tmp3[i] * 6.0;
      else if (2.0 * tmp3[i] < 1.0)
        clr[i] = tmp2;
      else if (3.0 * tmp3[i] < 2.0)
        clr[i] = (tmp1 + (tmp2 - tmp1) * ((2.0 / 3.0) - tmp3[i]) * 6.0);
      else
        clr[i] = tmp1;
    }

  color->red   = clr[0] * 255.0;
  color->green = clr[1] * 255.0;
  color->blue  = clr[2] * 255.0;
}

/**
 * mn_color_shade:
 * @color: a #MnColor
 * @factor: the shade factor to apply
 * @result: (out): return location for the shaded color
 *
 * Shades @color by @factor and saves the modified color into @result.
 */
void
mn_color_shade (const MnColor *color,
                     gdouble             factor,
                     MnColor       *result)
{
  float h, l, s;

  g_return_if_fail (color != NULL);
  g_return_if_fail (result != NULL);
  
  mn_color_to_hls (color, &h, &l, &s);

  l *= factor;
  if (l > 1.0)
    l = 1.0;
  else if (l < 0)
    l = 0;

  s *= factor;
  if (s > 1.0)
    s = 1.0;
  else if (s < 0)
    s = 0;
  
  mn_color_from_hls (result, h, l, s);

  result->alpha = color->alpha;
}

/**
 * mn_color_to_pixel:
 * @color: a #MnColor
 *
 * Converts @color into a packed 32 bit integer, containing
 * all the four 8 bit channels used by #MnColor.
 *
 * Return value: a packed color
 */
guint32
mn_color_to_pixel (const MnColor *color)
{
  g_return_val_if_fail (color != NULL, 0);
  
  return (color->alpha       |
          color->blue  << 8  |
          color->green << 16 |
          color->red   << 24);
}

/**
 * mn_color_from_pixel:
 * @color: (out): return location for a #MnColor
 * @pixel: a 32 bit packed integer containing a color
 *
 * Converts @pixel from the packed representation of a four 8 bit channel
 * color to a #MnColor.
 */
void
mn_color_from_pixel (MnColor *color,
			  guint32       pixel)
{
  g_return_if_fail (color != NULL);

  color->red   =  pixel >> 24;
  color->green = (pixel >> 16) & 0xff;
  color->blue  = (pixel >> 8)  & 0xff;
  color->alpha =  pixel        & 0xff;
}

/**
 * mn_color_from_string:
 * @color: (out): return location for a #MnColor
 * @str: a string specifiying a color (named color or #RRGGBBAA)
 *
 * Parses a string definition of a color, filling the
 * <structfield>red</structfield>, <structfield>green</structfield>, 
 * <structfield>blue</structfield> and <structfield>alpha</structfield> 
 * channels of @color. If alpha is not specified it will be set full opaque.
 *
 * The @color is not allocated.
 *
 * Return value: %TRUE if parsing succeeded.
 */
gboolean
mn_color_from_string (MnColor *color,
                           const gchar  *str)
{
  g_return_val_if_fail (color != NULL, FALSE);
  g_return_val_if_fail (str != NULL, FALSE);

  /* if the string contains a color encoded using the hexadecimal
   * notations (#rrggbbaa or #rgba) we attempt a rough pass at
   * parsing the color ourselves, as we need the alpha channel that
   * Pango can't retrieve.
   */
  if (str[0] == '#')
    {
      gint32 result;

      if (sscanf (str + 1, "%x", &result))
        {
          if (strlen (str) == 9)
            {
              /* #rrggbbaa */
              color->red   = (result >> 24) & 0xff;
              color->green = (result >> 16) & 0xff;
              color->blue  = (result >>  8) & 0xff;

              color->alpha = result & 0xff;

              return TRUE;
            }
          else if (strlen (str) == 5)
            {
              /* #rgba */
              color->red   = ((result >> 12) & 0xf);
              color->green = ((result >>  8) & 0xf);
              color->blue  = ((result >>  4) & 0xf);
              color->alpha = result & 0xf;

              color->red   = (color->red   << 4) | color->red;
              color->green = (color->green << 4) | color->green;
              color->blue  = (color->blue  << 4) | color->blue;
              color->alpha = (color->alpha << 4) | color->alpha;

              return TRUE;
            }
        }
    }

  return FALSE;
}

/**
 * mn_color_to_string:
 * @color: a #MnColor
 *
 * Returns a textual specification of @color in the hexadecimal form
 * <literal>&num;rrggbbaa</literal>, where <literal>r</literal>,
 * <literal>g</literal>, <literal>b</literal> and <literal>a</literal> are
 * hex digits representing the red, green, blue and alpha components
 * respectively.
 *
 * Return value: a newly-allocated text string
 */
gchar *
mn_color_to_string (const MnColor *color)
{
  g_return_val_if_fail (color != NULL, NULL);

  return g_strdup_printf ("#%02x%02x%02x%02x",
                          color->red,
                          color->green,
                          color->blue,
                          color->alpha);
}

/**
 * mn_color_equal:
 * @v1: a #MnColor
 * @v2: a #MnColor
 *
 * Compares two #MnColor<!-- -->s and checks if they are the same.
 *
 * This function can be passed to g_hash_table_new() as the @key_equal_func
 * parameter, when using #MnColor<!-- -->s as keys in a #GHashTable.
 *
 * Return value: %TRUE if the two colors are the same.
 */
gboolean
mn_color_equal (gconstpointer v1,
                     gconstpointer v2)
{
  const MnColor *a, *b;

  g_return_val_if_fail (v1 != NULL, FALSE);
  g_return_val_if_fail (v2 != NULL, FALSE);

  if (v1 == v2)
    return TRUE;

  a = v1;
  b = v2;

  return (a->red   == b->red   &&
          a->green == b->green &&
          a->blue  == b->blue  &&
          a->alpha == b->alpha);
}

/**
 * mn_color_hash:
 * @v: a #MnColor
 *
 * Converts a #MnColor to a hash value.
 *
 * This function can be passed to g_hash_table_new() as the @hash_func
 * parameter, when using #MnColor<!-- -->s as keys in a #GHashTable.
 *
 * Return value: a hash value corresponding to the color
 */
guint
mn_color_hash (gconstpointer v)
{
  return mn_color_to_pixel ((const MnColor *) v);
}

/**
 * mn_color_copy:
 * @color: a #MnColor
 *
 * Makes a copy of the color structure.  The result must be
 * freed using mn_color_free().
 *
 * Return value: an allocated copy of @color.
 */
MnColor *
mn_color_copy (const MnColor *color)
{
  if (G_LIKELY (color != NULL))
    return g_slice_dup (MnColor, color);

  return NULL;
}

/**
 * mn_color_free:
 * @color: a #MnColor
 *
 * Frees a color structure created with mn_color_copy().
 */
void
mn_color_free (MnColor *color)
{
  if (G_LIKELY (color != NULL))
    g_slice_free (MnColor, color);
}

/**
 * mn_color_new:
 * @red: red component of the color, between 0 and 255
 * @green: green component of the color, between 0 and 255
 * @blue: blue component of the color, between 0 and 255
 * @alpha: alpha component of the color, between 0 and 255
 *
 * Creates a new #MnColor with the given values.
 *
 * Return value: the newly allocated color. Use mn_color_free()
 *   when done
 */
MnColor *
mn_color_new (guint8 red,
                   guint8 green,
                   guint8 blue,
                   guint8 alpha)
{
  MnColor *color;

  color = g_slice_new (MnColor);

  color->red   = red;
  color->green = green;
  color->blue  = blue;
  color->alpha = alpha;

  return color;
}

static void
mn_value_transform_color_string (const GValue *src,
                                      GValue       *dest)
{
  gchar *string = mn_color_to_string (src->data[0].v_pointer);

  g_value_take_string (dest, string);
}

static void
mn_value_transform_string_color (const GValue *src,
                                      GValue       *dest)
{
  MnColor color = { 0, };

  mn_color_from_string (&color, g_value_get_string (src));

  mn_value_set_color (dest, &color);
}

GType
mn_color_get_type (void)
{
  static GType _mn_color_type = 0;
  
  if (G_UNLIKELY (_mn_color_type == 0))
    {
       _mn_color_type =
         g_boxed_type_register_static ("MnColor",
                                       (GBoxedCopyFunc) mn_color_copy,
                                       (GBoxedFreeFunc) mn_color_free);

       g_value_register_transform_func (_mn_color_type, G_TYPE_STRING,
                                        mn_value_transform_color_string);
       g_value_register_transform_func (G_TYPE_STRING, _mn_color_type,
                                        mn_value_transform_string_color);
    }

  return _mn_color_type;
}

static void
mn_value_init_color (GValue *value)
{
  value->data[0].v_pointer = NULL;
}

static void
mn_value_free_color (GValue *value)
{
  if (!(value->data[1].v_uint & G_VALUE_NOCOPY_CONTENTS))
    mn_color_free (value->data[0].v_pointer);
}

static void
mn_value_copy_color (const GValue *src,
                          GValue       *dest)
{
  dest->data[0].v_pointer = mn_color_copy (src->data[0].v_pointer);
}

static gpointer
mn_value_peek_color (const GValue *value)
{
  return value->data[0].v_pointer;
}

static gchar *
mn_value_collect_color (GValue      *value,
                             guint        n_collect_values,
                             GTypeCValue *collect_values,
                             guint        collect_flags)
{
  if (!collect_values[0].v_pointer)
      value->data[0].v_pointer = NULL;
  else
    {
      if (collect_flags & G_VALUE_NOCOPY_CONTENTS)
        {
          value->data[0].v_pointer = collect_values[0].v_pointer;
          value->data[1].v_uint = G_VALUE_NOCOPY_CONTENTS;
        }
      else
        {
          value->data[0].v_pointer =
            mn_color_copy (collect_values[0].v_pointer);
        }
    }

  return NULL;
}

static gchar *
mn_value_lcopy_color (const GValue *value,
                           guint         n_collect_values,
                           GTypeCValue  *collect_values,
                           guint         collect_flags)
{
  MnColor **color_p = collect_values[0].v_pointer;

  if (!color_p)
    return g_strdup_printf ("value location for '%s' passed as NULL",
                            G_VALUE_TYPE_NAME (value));

  if (!value->data[0].v_pointer)
    *color_p = NULL;
  else
    {
      if (collect_flags & G_VALUE_NOCOPY_CONTENTS)
        *color_p = value->data[0].v_pointer;
      else
        *color_p = mn_color_copy (value->data[0].v_pointer);
    }

  return NULL;
}

/**
 * mn_value_set_color:
 * @value: a #GValue initialized to #MN_TYPE_COLOR
 * @color: the color to set
 *
 * Sets @value to @color.
 */
void
mn_value_set_color (GValue             *value,
                         const MnColor *color)
{
  g_return_if_fail (MN_VALUE_HOLDS_COLOR (value));

  value->data[0].v_pointer = mn_color_copy (color);
}

/**
 * mn_value_get_color:
 * @value: a #GValue initialized to #MN_TYPE_COLOR
 *
 * Gets the #MnColor contained in @value.
 *
 * Return value: the colors inside the passed #GValue
 */
G_CONST_RETURN MnColor *
mn_value_get_color (const GValue *value)
{
  g_return_val_if_fail (MN_VALUE_HOLDS_COLOR (value), NULL);

  return value->data[0].v_pointer;
}

static void
param_color_init (GParamSpec *pspec)
{
  MnParamSpecColor *cspec = MN_PARAM_SPEC_COLOR (pspec);

  cspec->default_value = NULL;
}

static void
param_color_finalize (GParamSpec *pspec)
{
  MnParamSpecColor *cspec = MN_PARAM_SPEC_COLOR (pspec);

  mn_color_free (cspec->default_value);
}

static void
param_color_set_default (GParamSpec *pspec,
                        GValue     *value)
{
  value->data[0].v_pointer = MN_PARAM_SPEC_COLOR (pspec)->default_value;
  value->data[1].v_uint = G_VALUE_NOCOPY_CONTENTS;
}

static gint
param_color_values_cmp (GParamSpec   *pspec,
                        const GValue *value1,
                        const GValue *value2)
{
  guint32 color1, color2;

  color1 = mn_color_to_pixel (value1->data[0].v_pointer);
  color2 = mn_color_to_pixel (value2->data[0].v_pointer);

  if (color1 < color2)
    return -1;
  else if (color1 == color2)
    return 0;
  else
    return 1;
}

static const GTypeValueTable _mn_color_value_table = {
  mn_value_init_color,
  mn_value_free_color,
  mn_value_copy_color,
  mn_value_peek_color,
  "p",
  mn_value_collect_color,
  "p",
  mn_value_lcopy_color
};

GType
mn_param_color_get_type (void)
{
  static GType pspec_type = 0;

  if (G_UNLIKELY (pspec_type == 0))
    {
      const GParamSpecTypeInfo pspec_info = {
        sizeof (MnParamSpecColor),
        16,
        param_color_init,
        MN_TYPE_COLOR,
        param_color_finalize,
        param_color_set_default,
        NULL,
        param_color_values_cmp,
      };

      pspec_type = g_param_type_register_static ("MnParamSpecColor",
                                                 &pspec_info);
    }

  return pspec_type;
}

/**
 * mn_param_spec_color:
 * @name: name of the property
 * @nick: short name
 * @blurb: description (can be translatable)
 * @default_value: default value
 * @flags: flags for the param spec
 *
 * Creates a #GParamSpec for properties using #MnColor.
 *
 * Return value: the newly created #GParamSpec
 */
GParamSpec *
mn_param_spec_color (const gchar        *name,
                     const gchar        *nick,
                     const gchar        *blurb,
                     const MnColor      *default_value,
                     GParamFlags         flags)
{
  MnParamSpecColor *cspec;

  cspec = g_param_spec_internal (MN_TYPE_PARAM_COLOR,
                                 name, nick, blurb, flags);

  cspec->default_value = mn_color_copy (default_value);

  return G_PARAM_SPEC (cspec);
}
