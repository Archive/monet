/*
 * mn-palette.h: collection of colors
 *
 * Copyright 2009 Intel Corporation
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301
 * USA
 *
 * Author: Thomas Wood <thos@gnome.org>
 */

#ifndef MN_PALETTE_H
#define MN_PALETTE_H

#include "mn-color.h"
#include "mn-types.h"

typedef struct _MnPalette MnPalette;
typedef enum _MnPaletteIndex MnPaletteIndex;

enum _MnPaletteIndex
{
  MN_BG_COLOR,
  MN_FG_COLOR,
  MN_HIGHLIGHT_BG_COLOR,
  MN_HIGHLIGHT_FG_COLOR,
  MN_BORDER_COLOR
};


MnPalette* mn_palette_new       (void);
void       mn_palette_free      (MnPalette *palette);

const MnColor*   mn_palette_get_color (MnPalette      *palette,
                                       MnPaletteIndex  index,
                                       MnState         state);

void             mn_palette_set_color (MnPalette      *palette,
                                       MnPaletteIndex  index,
                                       MnState         state,
                                       const MnColor  *color);

#endif /* MN_PALETTE_H */
