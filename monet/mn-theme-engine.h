/* mn-theme-engine.h */

#ifndef _MN_THEME_ENGINE_H
#define _MN_THEME_ENGINE_H

#include <glib-object.h>
#include "mn-widget.h"
#include <cairo.h>

G_BEGIN_DECLS

#define MN_TYPE_THEME_ENGINE mn_theme_engine_get_type()

#define MN_THEME_ENGINE(obj) \
  (G_TYPE_CHECK_INSTANCE_CAST ((obj), \
  MN_TYPE_THEME_ENGINE, MnThemeEngine))

#define MN_THEME_ENGINE_CLASS(klass) \
  (G_TYPE_CHECK_CLASS_CAST ((klass), \
  MN_TYPE_THEME_ENGINE, MnThemeEngineClass))

#define MN_IS_THEME_ENGINE(obj) \
  (G_TYPE_CHECK_INSTANCE_TYPE ((obj), \
  MN_TYPE_THEME_ENGINE))

#define MN_IS_THEME_ENGINE_CLASS(klass) \
  (G_TYPE_CHECK_CLASS_TYPE ((klass), \
  MN_TYPE_THEME_ENGINE))

#define MN_THEME_ENGINE_GET_CLASS(obj) \
  (G_TYPE_INSTANCE_GET_CLASS ((obj), \
  MN_TYPE_THEME_ENGINE, MnThemeEngineClass))

typedef struct _MnThemeEngine MnThemeEngine;
typedef struct _MnThemeEngineClass MnThemeEngineClass;
typedef struct _MnThemeEnginePrivate MnThemeEnginePrivate;

struct _MnThemeEngine
{
  GObject parent;

  MnThemeEnginePrivate *priv;
};

struct _MnThemeEngineClass
{
  GObjectClass parent_class;

  /* vfuncs */

  gboolean (*paint_widget) (MnThemeEngine *engine,
                            MnWidget      *widget,
                            cairo_t       *cr);
};

GType mn_theme_engine_get_type (void) G_GNUC_CONST;

gboolean mn_theme_engine_paint_widget (MnThemeEngine *engine,
                                       MnWidget      *widget,
                                       cairo_t       *cr);

G_END_DECLS

#endif /* _MN_THEME_ENGINE_H */
