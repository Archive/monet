/* mn-widget.h */

#ifndef _MN_WIDGET_H
#define _MN_WIDGET_H

#include <glib-object.h>

G_BEGIN_DECLS

#define MN_TYPE_WIDGET mn_widget_get_type()

#define MN_WIDGET(obj) \
  (G_TYPE_CHECK_INSTANCE_CAST ((obj), \
  MN_TYPE_WIDGET, MnWidget))

#define MN_WIDGET_CLASS(klass) \
  (G_TYPE_CHECK_CLASS_CAST ((klass), \
  MN_TYPE_WIDGET, MnWidgetClass))

#define MN_IS_WIDGET(obj) \
  (G_TYPE_CHECK_INSTANCE_TYPE ((obj), \
  MN_TYPE_WIDGET))

#define MN_IS_WIDGET_CLASS(klass) \
  (G_TYPE_CHECK_CLASS_TYPE ((klass), \
  MN_TYPE_WIDGET))

#define MN_WIDGET_GET_CLASS(obj) \
  (G_TYPE_INSTANCE_GET_CLASS ((obj), \
  MN_TYPE_WIDGET, MnWidgetClass))

typedef struct _MnWidget MnWidget;
typedef struct _MnWidgetClass MnWidgetClass;
typedef struct _MnWidgetPrivate MnWidgetPrivate;

struct _MnWidget
{
  GObject parent;

  MnWidgetPrivate *priv;
};

struct _MnWidgetClass
{
  GObjectClass parent_class;
};

GType mn_widget_get_type (void) G_GNUC_CONST;

MnWidget *mn_widget_new (void);

G_END_DECLS

#endif /* _MN_WIDGET_H */
