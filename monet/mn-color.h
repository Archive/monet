/*
 * MnColor
 *
 * Based on clutter-color.h:
 *
 * Authored By: Matthew Allum  <mallum@openedhand.com>
 *              Emmanuele Bassi <ebassi@linux.intel.com>
 *
 * Copyright (C) 2006, 2007, 2008 OpenedHand
 * Copyright (C) 2009 Intel Corp.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef __MN_COLOR_H__
#define __MN_COLOR_H__

#include <glib-object.h>

G_BEGIN_DECLS

#define MN_TYPE_COLOR        (mn_color_get_type ())

typedef struct _MnColor MnColor;

/**
 * MnColor:
 * @red: red component, between 0 and 255
 * @green: green component, between 0 and 255
 * @blue: blue component, between 0 and 255
 * @alpha: alpha component, between 0 and 255
 *
 * Color representation.
 */
struct _MnColor
{
  /*< public >*/
  guint8 red;
  guint8 green;
  guint8 blue;
  
  guint8 alpha;
};

GType         mn_color_get_type   (void) G_GNUC_CONST;

MnColor *mn_color_new         (guint8              red,
                               guint8              green,
                               guint8              blue,
                               guint8              alpha);
MnColor *mn_color_copy        (const MnColor *color);
void          mn_color_free        (MnColor       *color);

void          mn_color_add         (const MnColor *a,
                                    const MnColor *b,
                                    MnColor       *result);
void          mn_color_subtract    (const MnColor *a,
                                    const MnColor *b,
                                    MnColor       *result);
void          mn_color_lighten     (const MnColor *color,
                                    MnColor       *result);
void          mn_color_darken      (const MnColor *color,
                                    MnColor       *result);
void          mn_color_shade       (const MnColor *color,
                                    gdouble             factor,
                                    MnColor       *result);

gchar *       mn_color_to_string   (const MnColor *color);
gboolean      mn_color_from_string (MnColor       *color,
                                    const gchar        *str);

void          mn_color_to_hls      (const MnColor *color,
                                    gfloat             *hue,
                                    gfloat             *luminance,
                                    gfloat             *saturation);
void          mn_color_from_hls    (MnColor       *color,
                                    gfloat              hue,
                                    gfloat              luminance,
                                    gfloat              saturation);

guint32       mn_color_to_pixel    (const MnColor *color);
void          mn_color_from_pixel  (MnColor       *color,
                                    guint32             pixel);

guint         mn_color_hash        (gconstpointer       v);
gboolean      mn_color_equal       (gconstpointer       v1,
                                    gconstpointer       v2);

#define MN_TYPE_PARAM_COLOR           (mn_param_color_get_type ())
#define MN_PARAM_SPEC_COLOR(pspec)    (G_TYPE_CHECK_INSTANCE_CAST ((pspec), MN_TYPE_PARAM_COLOR, MnParamSpecColor))
#define MN_IS_PARAM_SPEC_COLOR(pspec) (G_TYPE_CHECK_INSTANCE_TYPE ((pspec), MN_TYPE_PARAM_COLOR))

/**
 * MN_VALUE_HOLDS_COLOR:
 * @x: a #GValue
 *
 * Evaluates to %TRUE if @x holds a #MnColor<!-- -->.
 */
#define MN_VALUE_HOLDS_COLOR(x)       (G_VALUE_HOLDS ((x), MN_TYPE_COLOR))

typedef struct _MnParamSpecColor  MnParamSpecColor;

/**
  * MnParamSpecColor:
  * @default_value: default color value
  *
  * A #GParamSpec subclass for defining properties holding
  * a #MnColor.
  */
struct _MnParamSpecColor
{
  /*< private >*/
  GParamSpec    parent_instance;

  /*< public >*/
  MnColor *default_value;
};

void                         mn_value_set_color (GValue             *value,
                                                 const MnColor *color);
G_CONST_RETURN MnColor *mn_value_get_color (const GValue       *value);

GType       mn_param_color_get_type (void) G_GNUC_CONST;
GParamSpec *mn_param_spec_color     (const gchar        *name,
                                     const gchar        *nick,
                                     const gchar        *blurb,
                                     const MnColor      *default_value,
                                     GParamFlags         flags);

G_END_DECLS

#endif /* __MN_COLOR_H__ */
