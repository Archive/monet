/*
 * mn-palette.h: collection of colors
 *
 * Copyright 2009 Intel Corporation
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301
 * USA
 *
 * Author: Thomas Wood <thos@gnome.org>
 */

#include "mn-palette.h"

struct _MnPalette
{
  MnColor bg_colors[4];
  MnColor fg_colors[4];
  MnColor highlight_fg_colors[4];
  MnColor highlight_bg_colors[4];
  MnColor border_colors[4];
};

MnPalette *
mn_palette_new (void)
{
  return g_slice_new0 (MnPalette);
}

void
mn_palette_free (MnPalette *palette)
{
  g_slice_free (MnPalette, palette);
}

const MnColor *
mn_palette_get_color (MnPalette      *palette,
                      MnPaletteIndex  index,
                      MnState         state)
{
  switch (index)
    {
    case MN_FG_COLOR:
      return &palette->fg_colors[state];

    case MN_BG_COLOR:
      return &palette->bg_colors[state];

    case MN_HIGHLIGHT_BG_COLOR:
      return &palette->highlight_bg_colors[state];

    case MN_HIGHLIGHT_FG_COLOR:
      return &palette->highlight_fg_colors[state];

    case MN_BORDER_COLOR:
      return &palette->border_colors[state];

    default:
      return NULL;
    }
}

void
mn_palette_set_color (MnPalette      *palette,
                      MnPaletteIndex  index,
                      MnState         state,
                      const MnColor   *color)
{
  switch (index)
    {
    case MN_FG_COLOR:
      palette->fg_colors[state] = *color;
      break;

    case MN_BG_COLOR:
      palette->bg_colors[state] = *color;
      break;

    case MN_HIGHLIGHT_BG_COLOR:
      palette->highlight_bg_colors[state] = *color;
      break;

    case MN_HIGHLIGHT_FG_COLOR:
      palette->highlight_fg_colors[state] = *color;
      break;

    case MN_BORDER_COLOR:
      palette->border_colors[state] = *color;
      break;
    }
}
