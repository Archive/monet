/*
 * monet.h: a toolkit independent widget drawing library
 *
 * Copyright 2009 Intel Corporation
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301
 * USA
 *
 * Author: Thomas Wood <thos@gnome.org>
 */
#include <glib-object.h>

#ifndef MONET_H
#define MONET_H

#define MONET_H_INSIDE

#include <monet/mn-color.h>
#include <monet/mn-enum-types.h>
#include <monet/mn-marshal.h>
#include <monet/mn-parts.h>
#include <monet/mn-types.h>
#include <monet/mn-widget.h>
#include <monet/mn-theme-engine.h>
#include <monet/mn-palette.h>

#undef MONET_H_INSIDE

void monet_init (gint *argc, gchar ***argv);

#endif /* MONET_H */
