/*
 * mn-types.h: type definitions used in monet
 *
 * Copyright 2009 Intel Corporation
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301
 * USA
 *
 * Author: Thomas Wood <thos@gnome.org>
 */

#ifndef MN_TYPES_H
#define MN_TYPES_H

typedef enum
{
  MN_STATE_NORMAL,
  MN_STATE_HOVER,
  MN_STATE_ACTIVE,
  MN_STATE_DISABLED
} MnState;

typedef enum
{
  MN_FLAGS_FOCUS    = 1 << 0,
  MN_FLAGS_CHECKED  = 1 << 1
} MnFlags;

typedef struct
{
  gdouble x;
  gdouble y;
  gdouble width;
  gdouble height;
} MnRectangle;

typedef struct
{
  gdouble top;
  gdouble right;
  gdouble bottom;
  gdouble left;
} MnBorder;

#endif /* MN_TYPES_H */
