/* mn-widget.c */

#include "mn-widget.h"
#include "mn-palette.h"
#include "mn-color.h"

G_DEFINE_TYPE (MnWidget, mn_widget, G_TYPE_OBJECT)

#define WIDGET_PRIVATE(o) \
  (G_TYPE_INSTANCE_GET_PRIVATE ((o), MN_TYPE_WIDGET, MnWidgetPrivate))

struct _MnWidgetPrivate
{
  MnRectangle *area;
  MnPalette   *palette;
  MnBorder    *border;
  MnState      state;
};

enum
{
  PROP_AREA,
  PROP_STATE,
  PROP_BORDER,
  PROP_PALETTE
};

static void
mn_widget_get_property (GObject    *object,
                        guint       property_id,
                        GValue     *value,
                        GParamSpec *pspec)
{
  MnWidgetPrivate *priv = MN_WIDGET (object)->priv;

  switch (property_id)
    {
    case PROP_AREA:
      g_value_set_boxed (value, priv->area);
      break;

    case PROP_STATE:
      g_value_set_enum (value, priv->state);
      break;

    case PROP_BORDER:
      g_value_set_boxed (value, priv->border);
      break;

    case PROP_PALETTE:
      g_value_set_pointer (value, priv->palette);
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
    }
}

static void
mn_widget_set_property (GObject      *object,
                        guint         property_id,
                        const GValue *value,
                        GParamSpec   *pspec)
{
  MnWidgetPrivate *priv = MN_WIDGET (object)->priv;

  switch (property_id)
    {
    case PROP_AREA:
      priv->area = g_value_get_boxed (value);
      break;

    case PROP_STATE:
      priv->state = g_value_get_enum (value);
      break;

    case PROP_BORDER:
      priv->border = g_value_get_boxed (value);
      break;

    case PROP_PALETTE:
      priv->palette = g_value_get_boxed (value);
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
    }
}

static void
mn_widget_dispose (GObject *object)
{
  G_OBJECT_CLASS (mn_widget_parent_class)->dispose (object);
}

static void
mn_widget_finalize (GObject *object)
{
  MnWidgetPrivate *priv = MN_WIDGET (object)->priv;

  if (priv->area)
    {
      g_free (priv->area);
      priv->area = NULL;
    }

  if (priv->palette)
    {
      mn_palette_free (priv->palette);
      priv->palette = NULL;
    }

  if (priv->border)
    {
      g_free (priv->border);
      priv->border = NULL;
    }

  G_OBJECT_CLASS (mn_widget_parent_class)->finalize (object);
}

static void
mn_widget_class_init (MnWidgetClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);

  g_type_class_add_private (klass, sizeof (MnWidgetPrivate));

  object_class->get_property = mn_widget_get_property;
  object_class->set_property = mn_widget_set_property;
  object_class->dispose = mn_widget_dispose;
  object_class->finalize = mn_widget_finalize;
}

static void
mn_widget_init (MnWidget *self)
{
  self->priv = WIDGET_PRIVATE (self);
}

MnWidget *
mn_widget_new (void)
{
  return g_object_new (MN_TYPE_WIDGET, NULL);
}
