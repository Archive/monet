#!/bin/sh

if ! [ -d 'ac-helpers' ] ; then
  if ! mkdir 'ac-helpers' ; then
    echo "failed to create ac-helpers directory" >&2
    exit 1
  fi
fi


aclocal-1.8

autoconf
libtoolize --copy --force
automake-1.8 -a --copy

rm -rf autom4te.cache

./configure "$@"
