/*  Noop GTK engine: rcstyle.c
 *  
 *  Copyright (C) 2008  Benjamin Berg <benjamin@sipsolutions.net>
 *  
 *  This library is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU Library General Public
 *  License as published by the Free Software Foundation; either
 *  version 2 of the License, or (at your option) any later version.
 *
 *  This library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  Library General Public License for more details.
 *
 *  You should have received a copy of the GNU Library General Public
 *  License along with this library; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/


#include <gdk/gdk.h>
#include <glib.h>
#include "style.h"
#include "rcstyle.h"

static void      noop_rc_style_init         (NoopRcStyle      *style);
static void      noop_rc_style_class_init   (NoopRcStyleClass *klass);
static GtkStyle *noop_rc_style_create_style (GtkRcStyle       *rc_style);

static GtkRcStyleClass *parent_class;

GType noop_type_rc_style = 0;

void
noop_rc_style_register_type (GTypeModule *module)
{
  if (!noop_type_rc_style) {
    static const GTypeInfo object_info =
    {
      sizeof (NoopRcStyleClass),
      (GBaseInitFunc) NULL,
      (GBaseFinalizeFunc) NULL,
      (GClassInitFunc) noop_rc_style_class_init,
      NULL,           /* class_finalize */
      NULL,           /* class_data */
      sizeof (NoopRcStyle),
      0,              /* n_preallocs */
      (GInstanceInitFunc) noop_rc_style_init,
      NULL
    };

    noop_type_rc_style = g_type_module_register_type (module,
                             GTK_TYPE_RC_STYLE,
                             "NoopRcStyle",
                             &object_info, 0);
  }
}

static void
noop_rc_style_init (NoopRcStyle *style)
{
  /* Initilize any variables of the NoopRcStyle object here */
}

static void
noop_rc_style_finalize (NoopRcStyle *style)
{
  /* Clean up any data from the RC style in here. */
}

static void
noop_rc_style_merge (GtkRcStyle *dest, GtkRcStyle *src)
{
  parent_class->merge (dest, src);

  if (!NOOP_IS_RC_STYLE (src))
    return;

  /* This function "merges" two RC styles. All matched styles from the
   * gtkrc are merged together to form the final style.
   *
   * Note about the order: The merging happens in REVERSE
   * (at least to what I woulde expect).
   * Often one will want to keep track of where an option has been set
   * and use the version that was set last. For this one needs a bitfield
   * and some code to copy when neccessary. (You can refere to eg. Clearlooks
   * or other engines in gtk-engines of how this can be handled.) */  
}

static guint
noop_rc_style_parse (GtkRcStyle   *rc_style,
                     GtkSettings  *settings,
                     GScanner     *scanner)
{
  NoopRcStyle *noop_rc_style = NOOP_RC_STYLE (rc_style);

  return G_TOKEN_NONE;
}

static void
noop_rc_style_class_init (NoopRcStyleClass *klass)
{
  GtkRcStyleClass *rc_style_class = GTK_RC_STYLE_CLASS (klass);
  GObjectClass    *g_object_class = G_OBJECT_CLASS (klass);

  parent_class = g_type_class_peek_parent (klass);

  /* These are the functions we override from the GtkRcStyle class.
   * We need to override at least create_style. (Uncomment the other
   * two to override them) */
  rc_style_class->create_style = noop_rc_style_create_style;
  /*rc_style_class->merge = noop_rc_style_merge;
  rc_style_class->parse        = noop_rc_style_parse;

  g_object_class->finalize     = noop_rc_style_finalize;*/
}

/* Create an empty style suitable to this RC style
 */
static GtkStyle *
noop_rc_style_create_style (GtkRcStyle *rc_style)
{
  return GTK_STYLE (g_object_new (NOOP_TYPE_STYLE, NULL));
}


