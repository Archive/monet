/*  Noop GTK engine: style.c
 *
 *  Copyright (C) 2008  Benjamin Berg <benjamin@sipsolutions.net>
 *
 *  This library is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU Library General Public
 *  License as published by the Free Software Foundation; either
 *  version 2 of the License, or (at your option) any later version.
 *
 *  This library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  Library General Public License for more details.
 *
 *  You should have received a copy of the GNU Library General Public
 *  License along with this library; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

#include <string.h>
#include <gtk/gtk.h>
#include "style.h"

static GtkStyleClass *noop_style_parent_class;

static void
noop_draw_hline  (GtkStyle        *style,
                  GdkWindow       *window,
                  GtkStateType     state_type,
                  GdkRectangle    *area,
                  GtkWidget       *widget,
                  const gchar     *detail,
                  gint             x1,
                  gint             x2,
                  gint             y)
{
  noop_style_parent_class->draw_vline (style, window, state_type, area, widget, detail, x1, x2, y);
}
/*##################################*/

static void
noop_draw_vline  (GtkStyle        *style,
                  GdkWindow       *window,
                  GtkStateType     state_type,
                  GdkRectangle    *area,
                  GtkWidget       *widget,
                  const gchar     *detail,
                  gint             y1,
                  gint             y2,
                  gint             x)
{
  noop_style_parent_class->draw_vline (style, window, state_type, area, widget, detail, y1, y2, x);
}
/*##################################*/

static void
noop_draw_shadow (GtkStyle        *style,
                  GdkWindow       *window,
                  GtkStateType     state_type,
                  GtkShadowType    shadow_type,
                  GdkRectangle    *area,
                  GtkWidget       *widget,
                  const gchar     *detail,
                  gint             x,
                  gint             y,
                  gint             width,
                  gint             height)
{
  noop_style_parent_class->draw_shadow (style, window, state_type, shadow_type, area, widget, detail, x, y, width, height);
}
/*##################################*/

static void
noop_draw_arrow  (GtkStyle        *style,
                  GdkWindow       *window,
                  GtkStateType     state_type,
                  GtkShadowType    shadow_type,
                  GdkRectangle    *area,
                  GtkWidget       *widget,
                  const gchar     *detail,
                  GtkArrowType     arrow_type,
                  gboolean         fill,
                  gint             x,
                  gint             y,
                  gint             width,
                  gint             height)
{
  noop_style_parent_class->draw_arrow (style, window, state_type, shadow_type, area, widget, detail, arrow_type, fill, x, y, width, height);
}

/*##################################*/

static void
noop_draw_box    (GtkStyle        *style,
                  GdkWindow       *window,
                  GtkStateType     state_type,
                  GtkShadowType    shadow_type,
                  GdkRectangle    *area,
                  GtkWidget       *widget,
                  const gchar     *detail,
                  gint             x,
                  gint             y,
                  gint             width,
                  gint             height)
{
  noop_style_parent_class->draw_box (style, window, state_type, shadow_type, area, widget, detail, x, y, width, height);
}
/*##################################*/

static void
noop_draw_flat_box (GtkStyle        *style,
                    GdkWindow       *window,
                    GtkStateType     state_type,
                    GtkShadowType    shadow_type,
                    GdkRectangle    *area,
                    GtkWidget       *widget,
                    const gchar     *detail,
                    gint             x,
                    gint             y,
                    gint             width,
                    gint             height)
{
  noop_style_parent_class->draw_flat_box (style, window, state_type, shadow_type, area, widget, detail, x, y, width, height);
}
/*##################################*/

static void
noop_draw_check  (GtkStyle        *style,
                  GdkWindow       *window,
                  GtkStateType     state_type,
                  GtkShadowType    shadow_type,
                  GdkRectangle    *area,
                  GtkWidget       *widget,
                  const gchar     *detail,
                  gint             x,
                  gint             y,
                  gint             width,
                  gint             height)
{
  noop_style_parent_class->draw_check (style, window, state_type, shadow_type, area, widget, detail, x, y, width, height);
}
/*##################################*/

static void
noop_draw_option (GtkStyle        *style,
                  GdkWindow       *window,
                  GtkStateType     state_type,
                  GtkShadowType    shadow_type,
                  GdkRectangle    *area,
                  GtkWidget       *widget,
                  const gchar     *detail,
                  gint             x,
                  gint             y,
                  gint             width,
                  gint             height)
{
  noop_style_parent_class->draw_option (style, window, state_type, shadow_type, area, widget, detail, x, y, width, height);
}
/*##################################*/

static void
noop_draw_tab (GtkStyle        *style,
               GdkWindow       *window,
               GtkStateType     state_type,
               GtkShadowType    shadow_type,
               GdkRectangle    *area,
               GtkWidget       *widget,
               const gchar     *detail,
               gint             x,
               gint             y,
               gint             width,
               gint             height)
{
  noop_style_parent_class->draw_tab (style, window, state_type, shadow_type, area, widget, detail, x, y, width, height);
}
/*##################################*/

static void
noop_draw_shadow_gap (GtkStyle        *style,
                      GdkWindow       *window,
                      GtkStateType     state_type,
                      GtkShadowType    shadow_type,
                      GdkRectangle    *area,
                      GtkWidget       *widget,
                      const gchar     *detail,
                      gint             x,
                      gint             y,
                      gint             width,
                      gint             height,
                      GtkPositionType  gap_side,
                      gint             gap_x,
                      gint             gap_width)
{
  noop_style_parent_class->draw_shadow_gap (style, window, state_type, shadow_type, area, widget, detail, x, y, width, height, gap_side, gap_x, gap_width);
}
/*##################################*/

static void
noop_draw_box_gap(GtkStyle        *style,
                  GdkWindow       *window,
                  GtkStateType     state_type,
                  GtkShadowType    shadow_type,
                  GdkRectangle    *area,
                  GtkWidget       *widget,
                  const gchar     *detail,
                  gint             x,
                  gint             y,
                  gint             width,
                  gint             height,
                  GtkPositionType  gap_side,
                  gint             gap_x,
                  gint             gap_width)
{
  noop_style_parent_class->draw_shadow_gap (style, window, state_type, shadow_type, area, widget, detail, x, y, width, height, gap_side, gap_x, gap_width);
}
/*##################################*/

static void
noop_draw_extension  (GtkStyle        *style,
                      GdkWindow       *window,
                      GtkStateType     state_type,
                      GtkShadowType    shadow_type,
                      GdkRectangle    *area,
                      GtkWidget       *widget,
                      const gchar     *detail,
                      gint             x,
                      gint             y,
                      gint             width,
                      gint             height,
                      GtkPositionType  gap_side)
{
  noop_style_parent_class->draw_extension (style, window, state_type, shadow_type, area, widget, detail, x, y, width, height, gap_side);
}
/*##################################*/

static void
noop_draw_focus  (GtkStyle        *style,
                  GdkWindow       *window,
                  GtkStateType     state_type,
                  GdkRectangle    *area,
                  GtkWidget       *widget,
                  const gchar     *detail,
                  gint             x,
                  gint             y,
                  gint             width,
                  gint             height)
{
  noop_style_parent_class->draw_focus (style, window, state_type, area, widget, detail, x, y, width, height);
}
/*##################################*/

static void
noop_draw_slider (GtkStyle        *style,
                  GdkWindow       *window,
                  GtkStateType     state_type,
                  GtkShadowType    shadow_type,
                  GdkRectangle    *area,
                  GtkWidget       *widget,
                  const gchar     *detail,
                  gint             x,
                  gint             y,
                  gint             width,
                  gint             height,
                  GtkOrientation   orientation)
{
  noop_style_parent_class->draw_slider (style, window, state_type, shadow_type, area, widget, detail, x, y, width, height, orientation);
}
/*##################################*/

static void
noop_draw_handle (GtkStyle        *style,
                  GdkWindow       *window,
                  GtkStateType     state_type,
                  GtkShadowType    shadow_type,
                  GdkRectangle    *area,
                  GtkWidget       *widget,
                  const gchar     *detail,
                  gint             x,
                  gint             y,
                  gint             width,
                  gint             height,
                  GtkOrientation   orientation)
{
  noop_style_parent_class->draw_handle (style, window, state_type, shadow_type, area, widget, detail, x, y, width, height, orientation);
}
/*##################################*/

static void
noop_draw_expander (GtkStyle        *style,
                    GdkWindow       *window,
                    GtkStateType     state_type,
                    GdkRectangle    *area,
                    GtkWidget       *widget,
                    const gchar     *detail,
                    gint             x,
                    gint             y,
                    GtkExpanderStyle expander_style)
{
  noop_style_parent_class->draw_expander (style, window, state_type, area, widget, detail, x, y, expander_style);
}
/*##################################*/

static void
noop_draw_resize_grip (GtkStyle       *style,
                       GdkWindow      *window,
                       GtkStateType    state_type,
                       GdkRectangle   *area,
                       GtkWidget      *widget,
                       const gchar    *detail,
                       GdkWindowEdge   edge,
                       gint            x,
                       gint            y,
                       gint            width,
                       gint            height)
{
  noop_style_parent_class->draw_resize_grip (style, window, state_type, area, widget, detail, edge, x, y, width, height);
}


static void
noop_style_class_init (NoopStyleClass * klass)
{
  GtkStyleClass *style_class = GTK_STYLE_CLASS (klass);

  noop_style_parent_class = g_type_class_peek_parent (klass);

  /* All functions that have prototypes here are not commented out. */
  style_class->draw_hline       = noop_draw_hline;
  style_class->draw_vline       = noop_draw_vline;
  style_class->draw_shadow      = noop_draw_shadow;
  style_class->draw_arrow       = noop_draw_arrow;
  style_class->draw_box         = noop_draw_box;
  style_class->draw_flat_box    = noop_draw_flat_box;
  style_class->draw_check       = noop_draw_check;
  style_class->draw_option      = noop_draw_option;
  style_class->draw_tab         = noop_draw_tab;
  style_class->draw_shadow_gap  = noop_draw_shadow_gap;
  style_class->draw_box_gap     = noop_draw_box_gap;
  style_class->draw_extension   = noop_draw_extension;
  style_class->draw_focus       = noop_draw_focus;
  style_class->draw_slider      = noop_draw_slider;
  style_class->draw_handle      = noop_draw_handle;
  style_class->draw_expander    = noop_draw_expander;
  style_class->draw_resize_grip = noop_draw_resize_grip;
  /*style_class->render_icon      = noop_render_icon;
  style_class->draw_layout      = noop_draw_layout; */

  /* There are some more functions in GtkStyleClass that may be overridden.
   * See gtkstyle.h in the GTK+ sources for a list and help for most of them. */
}

void
noop_style_register_type (GTypeModule * module)
{
  if (!noop_type_style) {
    static const GTypeInfo object_info =
    {
      sizeof (NoopStyleClass),
      (GBaseInitFunc) NULL,
      (GBaseFinalizeFunc) NULL,
      (GClassInitFunc) noop_style_class_init,
      NULL,     /* class_finalize */
      NULL,     /* class_data */
      sizeof (NoopStyle),
      0,        /* n_preallocs */
      (GInstanceInitFunc) NULL,
      NULL
    };

    noop_type_style = g_type_module_register_type (module,
                                                         GTK_TYPE_STYLE,
                                                         "NoopStyle",
                                                         &object_info, 0);
  }
}
