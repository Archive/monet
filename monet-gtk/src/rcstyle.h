/*  Noop GTK engine: rcstyle.h
 *
 *  Copyright (C) 2008  Benjamin Berg <benjamin@sipsolutions.net>
 *
 *  This library is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU Library General Public
 *  License as published by the Free Software Foundation; either
 *  version 2 of the License, or (at your option) any later version.
 *
 *  This library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  Library General Publi License for more details.
 *
 *  You should have received a copy of the GNU Library General Public
 *  License along with this library; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

#ifndef __RCSTYLE_H
#define __RCSTYLE_H

#include <gtk/gtk.h>
#include <gtk/gtkrc.h>

typedef struct _NoopRcStyle NoopRcStyle;
typedef struct _NoopRcStyleClass NoopRcStyleClass;


GType noop_type_rc_style G_GNUC_INTERNAL;

#define NOOP_TYPE_RC_STYLE              noop_type_rc_style
#define NOOP_RC_STYLE(object)           (G_TYPE_CHECK_INSTANCE_CAST ((object), NOOP_TYPE_RC_STYLE, NoopRcStyle))
#define NOOP_RC_STYLE_CLASS(klass)      (G_TYPE_CHECK_CLASS_CAST ((klass), NOOP_TYPE_RC_STYLE, NoopRcStyleClass))
#define NOOP_IS_RC_STYLE(object)        (G_TYPE_CHECK_INSTANCE_TYPE ((object), NOOP_TYPE_RC_STYLE))
#define NOOP_IS_RC_STYLE_CLASS(klass)   (G_TYPE_CHECK_CLASS_TYPE ((klass), NOOP_TYPE_RC_STYLE))
#define NOOP_RC_STYLE_GET_CLASS(obj)    (G_TYPE_INSTANCE_GET_CLASS ((obj), NOOP_TYPE_RC_STYLE, NoopRcStyleClass))

struct _NoopRcStyle
{
  GtkRcStyle parent_instance;
};

struct _NoopRcStyleClass
{
  GtkRcStyleClass parent_class;
};

void noop_rc_style_register_type (GTypeModule *module) G_GNUC_INTERNAL;
void noop_rc_style_load_groups (NoopRcStyle * rcstyle) G_GNUC_INTERNAL;

void noop_cleanup_everything (void) G_GNUC_INTERNAL;

#endif /* __RCSTYLE_H */
