/*  noop GTK engine: style.h
 *
 *  Copyright (C) 2008  Benjamin Berg <benjamin@sipsolutions.net>
 *
 *  This library is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU Library General Public
 *  License as published by the Free Software Foundation; either
 *  version 2 of the License, or (at your option) any later version.
 *
 *  This library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  Library General Public License for more details.
 *
 *  You should have received a copy of the GNU Library General Public
 *  License along with this library; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

#include <gtk/gtkstyle.h>

typedef struct _NoopStyle NoopStyle;
typedef struct _NoopStyleClass NoopStyleClass;


GType noop_type_style G_GNUC_INTERNAL;

#define NOOP_TYPE_STYLE              noop_type_style
#define NOOP_STYLE(object)           (G_TYPE_CHECK_INSTANCE_CAST ((object), NOOP_TYPE_STYLE, NoopStyle))
#define NOOP_STYLE_CLASS(klass)      (G_TYPE_CHECK_CLASS_CAST ((klass), NOOP_TYPE_STYLE, NoopStyleClass))
#define NOOP_IS_STYLE(object)        (G_TYPE_CHECK_INSTANCE_TYPE ((object), NOOP_TYPE_STYLE))
#define NOOP_IS_STYLE_CLASS(klass)   (G_TYPE_CHECK_CLASS_TYPE ((klass), NOOP_TYPE_STYLE))
#define NOOP_STYLE_GET_CLASS(obj)    (G_TYPE_INSTANCE_GET_CLASS ((obj), NOOP_TYPE_STYLE, NoopStyleClass))

struct _NoopStyle
{
  GtkStyle parent_instance;
};

struct _NoopStyleClass
{
  GtkStyleClass parent_class;
};

void noop_style_register_type (GTypeModule *module) G_GNUC_INTERNAL;

